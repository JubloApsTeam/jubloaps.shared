﻿using System;
using PostSharp.Aspects;
using PostSharp.Patterns.Contracts;
using PostSharp.Reflection;

namespace JubloAps.Shared.Aspects
{
    // ReSharper disable once UnusedMember.Global
    public class GuidNotNullAttribute : LocationContractAttribute, ILocationValidationAspect<Guid>
    {
        // ReSharper disable once UnusedMember.Global
        public new const string ErrorMessage = "GuidNotNull";

        protected override string GetErrorMessage()
        {
            return "Value Guid {2} must have a non-00000000-0000-0000-0000-000000000000 value.";
        }

        public Exception ValidateValue(Guid value, string locationName, LocationKind locationKind, LocationValidationContext context)
        {
            if (value == Guid.Empty)
                return this.CreateArgumentNullException(value, locationName, locationKind);

            return null;
        }
    }
}
