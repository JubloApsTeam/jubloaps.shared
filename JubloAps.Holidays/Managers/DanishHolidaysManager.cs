﻿using System;
using System.Collections.Generic;

// ReSharper disable UnusedMember.Global

namespace JubloAps.Holidays.Managers
{
    internal class DanishHolidaysManager
    {
	    private readonly ChristianHolidaysManager _christianHolidaysManager = new ChristianHolidaysManager();

        private  int _year;
	    internal DanishHolidaysManager()
	    {
		    this._year = DateTime.Now.Year;
	    }

	    internal DanishHolidaysManager(int year)
	    {
			this.SetYear(year);
	    }

	    internal void SetYear(int year)
        {
            this.ValidateYear(year);

            this._year = year;
        }

        internal DateTime NewYearDay()
	    {
			return new DateTime(this._year, 1, 1).Date;
        }

	    internal DateTime NewYearDay(int year)
	    {
		    this.ValidateYear(year);
            return new DateTime(year, 1, 1).Date;
	    }
        internal DateTime GetMaundyThursday()
	    {
		    return this._christianHolidaysManager.GetMaundyThursday(this._year);
	    }

	    internal DateTime GetMaundyThursday(int year)
	    {
		    this.ValidateYear(year);
            return this._christianHolidaysManager.GetMaundyThursday(year);
	    }

        internal DateTime GetGoodFriday()
	    {
		    return this._christianHolidaysManager.GetGoodFriday(this._year);
	    }

	    internal DateTime GetGoodFriday(int year)
	    {
		    this.ValidateYear(year);
            return this._christianHolidaysManager.GetGoodFriday(year);
	    }

        internal DateTime GetEasterMonday()
	    {
		    return this._christianHolidaysManager.GetEasterMonday(this._year);
	    }

	    internal DateTime GetEasterMonday(int year)
	    {
		    this.ValidateYear(year);
            return this._christianHolidaysManager.GetEasterMonday(year);
	    }


        internal DateTime GetGreatPrayerDay()
	    {
		    return this._christianHolidaysManager.GetGreatPrayerDay(this._year);
	    }

	    internal DateTime GetGreatPrayerDay(int year)
	    {
		    this.ValidateYear(year);
            return this._christianHolidaysManager.GetGreatPrayerDay(year);
	    }


        internal DateTime GetAscensionDay()
	    {
		    return this._christianHolidaysManager.GetAscensionDay(this._year);
	    }

	    internal DateTime GetAscensionDay(int year)
	    {
		    this.ValidateYear(year);
            return this._christianHolidaysManager.GetAscensionDay(year);
	    }

        internal DateTime GetWhitSunday()
	    {
		    return this._christianHolidaysManager.GetWhitSunday(this._year);
	    }


	    internal DateTime GetWhitSunday(int year)
	    {
		    this.ValidateYear(year);
            return this._christianHolidaysManager.GetWhitSunday(year);
	    }

        internal DateTime GetWhitMonday()
	    {
		    return this._christianHolidaysManager.GetWhitSunday(this._year);
	    }

	    internal DateTime GetWhitMonday(int year)
	    {
		    this.ValidateYear(year);
            return this._christianHolidaysManager.GetWhitSunday(year);
	    }

        internal DateTime GetEasterSunday()
	    {
		    return this._christianHolidaysManager.EasterSunday(this._year);
	    }

	    internal DateTime GetEasterSunday(int year)
	    {
		    this.ValidateYear(year);
            return this._christianHolidaysManager.EasterSunday(year);
	    }


        internal DateTime GetChristmasDay()
	    {
		    return new DateTime(this._year, 12, 25).Date;
	    }

	    internal DateTime GetChristmasDay(int year)
	    {
		    this.ValidateYear(year);
            return new DateTime(year, 12, 25).Date;
	    }

        internal DateTime GetSecondChristmasDay()
	    {
		    return new DateTime(this._year, 12, 26).Date;
	    }

	    internal DateTime GetSecondChristmasDay(int year)
	    {
		    this.ValidateYear(year);
            return new DateTime(year, 12, 26).Date;
	    }

        internal List<DateTime> GetHolidays()
	    {
		    return this.GetHolidays(this._year, this._year);

	    }

	    internal List<DateTime> GetHolidays(int year)
	    {
		    this.ValidateYear(year);
            return this.GetHolidays(year, year);

	    }

        internal List<DateTime> GetHolidays(int fromYear, int toYear)
	    {
		    this.ValidateYear(fromYear);
		    this.ValidateYear(toYear);

            var result = new List<DateTime>();

            for (int year = fromYear; year <= toYear; year++)
		    {
				result.Add(this.NewYearDay(year));
			    result.Add(this.GetMaundyThursday(year));
			    result.Add(this.GetGoodFriday(year));
			    result.Add(this.GetEasterMonday(year));
			    result.Add(this.GetGreatPrayerDay(year));
			    result.Add(this.GetAscensionDay(year));
			    result.Add(this.GetWhitSunday(year));
			    result.Add(this.GetWhitMonday(year));
			    result.Add(this.GetEasterSunday(year));
			    result.Add(this.GetChristmasDay(year));
			    result.Add(this.GetSecondChristmasDay(year));
            }

		    return result;
	    }

	    internal bool Holiday(int day,int month, int year)
	    {
			var date = new DateTime(year,month,day);
		    return this.GetHolidays(year).Contains(date);
	    }

        internal bool Holiday(DateTime date)
	    {
            return this.GetHolidays(date.Year).Contains(date);
	    }

	    internal void ValidateYear(int year)
	    {
		    if (year > DateTime.MaxValue.Year || year < DateTime.MinValue.Year)
		    {
			    throw new ArgumentOutOfRangeException($"DanishHolidays - Year: {year} is out of range (Between {DateTime.MinValue.Year} and { DateTime.MaxValue.Year} )");
		    }
	    }

    }
}
