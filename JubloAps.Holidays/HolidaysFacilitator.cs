﻿using System;
using System.Collections.Generic;
using JubloAps.Holidays.Managers;

// ReSharper disable UnusedMember.Global

namespace JubloAps.Holidays
{
    public class HolidaysFacilitator
    {
	    readonly DanishHolidaysManager _danishHolidaysManager = new DanishHolidaysManager();

        private int _year;
	    public HolidaysFacilitator()
        {
            this._year = DateTime.Now.Year;
        }

	    public HolidaysFacilitator(int year)
        {
            this._danishHolidaysManager.ValidateYear(year);
	        this._year = year;
        }

	    public void SetYear(int year)
        {
	        this._year = year;
        }

	    public DateTime NewYearDay()
	    {
		    return this.NewYearDay(this._year);
	    }

	    public DateTime NewYearDay(int year)
        {
		        return this._danishHolidaysManager.NewYearDay(year);

        }
	    public DateTime GetMaundyThursday()
        {
            return this.GetMaundyThursday(this._year);
        }

	    public DateTime GetMaundyThursday(int year)
        {
		        return this._danishHolidaysManager.GetMaundyThursday(year);
        }

	    public DateTime GetGoodFriday()
        {
            return this.GetGoodFriday(this._year);
        }

	    public DateTime GetGoodFriday(int year)
        {
		        return this._danishHolidaysManager.GetGoodFriday(year);
        }

	    public DateTime GetEasterMonday()
        {
            return this.GetEasterMonday(this._year);
        }

	    public DateTime GetEasterMonday(int year)
        {
		        return this._danishHolidaysManager.GetEasterMonday(year);
        }


	    public DateTime GetGreatPrayerDay()
        {
            return this.GetGreatPrayerDay(this._year);
        }

	    public DateTime GetGreatPrayerDay(int year)
        {
		        return this._danishHolidaysManager.GetGreatPrayerDay(year);
        }


	    public DateTime GetAscensionDay()
        {
            return this.GetAscensionDay(this._year);
        }

	    public DateTime GetAscensionDay(int year)
        {
		        return this._danishHolidaysManager.GetAscensionDay(year);
        }

	    public DateTime GetWhitSunday()
        {
            return this.GetWhitSunday(this._year);
        }


	    public DateTime GetWhitSunday(int year)
        {
            return this._danishHolidaysManager.GetWhitSunday(year);
        }

	    public DateTime GetWhitMonday()
        {
		        return this.GetWhitSunday(this._year);
        }

	    public DateTime GetWhitMonday(int year)
        {
		        return this._danishHolidaysManager.GetWhitSunday(year);
        }

	    public DateTime GetEasterSunday()
        {
            return this.GetEasterSunday(this._year);
        }

	    public DateTime GetEasterSunday(int year)
        {
		        return this._danishHolidaysManager.GetEasterSunday(year);
        }


	    public DateTime GetChristmasDay()
        {
	        return this.GetChristmasDay(this._year);
        }

	    public DateTime GetChristmasDay(int year)
        {
		        return this._danishHolidaysManager.GetChristmasDay(year);
        }

	    public DateTime GetSecondChristmasDay()
        {
	        return this.GetSecondChristmasDay(this._year);
        }

	    public DateTime GetSecondChristmasDay(int year)
        {
		        return this._danishHolidaysManager.GetSecondChristmasDay(year);
        }

	    public List<DateTime> GetHolidays()
        {
            return this.GetHolidays(this._year, this._year);

        }

	    public List<DateTime> GetHolidays(int year)
        {
			return this.GetHolidays(year, year);

        }

	    public List<DateTime> GetHolidays(int fromYear, int toYear)
        {
		        return this._danishHolidaysManager.GetHolidays(fromYear, toYear);
        }

	    public bool Holiday(int day, int month, int year)
        {
            var date = new DateTime(year, month, day);
            return this.GetHolidays(year).Contains(date);
        }

        public bool Holiday(DateTime date)
        {
            return this.GetHolidays(date.Year).Contains(date);
        }
    }
}
