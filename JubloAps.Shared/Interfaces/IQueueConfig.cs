﻿namespace JubloAps.Shared.Interfaces
{
    public interface IQueueConfig
    {
        string QueueName();
        string ConnectionString();
    }
}
