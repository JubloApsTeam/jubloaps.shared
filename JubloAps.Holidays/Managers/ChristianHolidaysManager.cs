﻿using System;

namespace JubloAps.Holidays.Managers
{
    internal class ChristianHolidaysManager
    {
		/// <summary>
        /// (Skærtorsdag) - Gets the maundy thursday of the specified <paramref name="year"/>.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        /// <remarks>The Thursday before Easter; commemorates the Last Supper.</remarks>
        internal DateTime GetMaundyThursday(int year)
        {
            if (year > DateTime.MaxValue.Year)
            {
                throw new ArgumentException("year");
            }
            return this.CalculateEasterSunday(year).AddDays(-3);
        }
        /// <summary>
        /// (Langfredag) - Gets the good friday of the specified <paramref name="year"/>. 
        /// </summary>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        /// <remarks>Friday before Easter.</remarks>
        internal DateTime GetGoodFriday(int year)
        {
            if (year > DateTime.MaxValue.Year)
            {
                throw new ArgumentException("year");
            }
            return this.GetMaundyThursday(year).AddDays(1);
        }

        /// <summary>
        /// (Anden påskedag) - Gets the easter monday of the specified <paramref name="year"/>.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        internal DateTime GetEasterMonday(int year)
        {
            if (year > DateTime.MaxValue.Year)
            {
                throw new ArgumentException("year");
            }
            return this.CalculateEasterSunday(year).AddDays(1);
        }

        /// <summary>
        /// (Store bededag) - Gets the great prayer day of the specified <paramref name="year"/>.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        /// <remarks>This is a specific danish holyday.</remarks>
        internal DateTime GetGreatPrayerDay(int year)
        {
            if (year > DateTime.MaxValue.Year)
            {
                throw new ArgumentException("year");
            }
            //fourth friday after easter.
            return this.CalculateEasterSunday(year).AddDays(5 + 3 * 7);
        }

        /// <summary>
        /// (Kristi himmelfartsdag) - Gets the ascension day of the specified <paramref name="year"/>.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        /// <remarks>Celebration of the Ascension of Christ into heaven; observed on the 40th day after Easter</remarks>
        internal DateTime GetAscensionDay(int year)
        {
            if (year > DateTime.MaxValue.Year)
            {
                throw new ArgumentException("year");
            }
            //sixth thursday after easter.
            return this.CalculateEasterSunday(year).AddDays(39);
        }

        /// <summary>
        /// (Pinsedag) - Gets the whit sunday of the specified <paramref name="year"/>.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        /// <remarks>Seventh Sunday after Easter; commemorates the emanation of the Holy Spirit to the Apostles; a quarter day in Scotland.</remarks>
        internal DateTime GetWhitSunday(int year)
        {
            if (year > DateTime.MaxValue.Year)
            {
                throw new ArgumentException("year");
            }
            return this.CalculateEasterSunday(year).AddDays(7 * 7);
        }

        /// <summary>
        /// (Anden pinsedag) - Gets the whit monday of the specified <paramref name="year"/>.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <returns></returns>
        /// <remarks>The day after Whitsunday; a legal holiday in England and Wales and Ireland.</remarks>
        internal DateTime GetWhitMonday(int year)
        {
            if (year > DateTime.MaxValue.Year)
            {
                throw new ArgumentException("year");
            }
            return this.GetWhitSunday(year).AddDays(1);
        }

	    internal DateTime EasterSunday(int year)
	    {
		    return this.CalculateEasterSunday(year);
	    }

        /// <summary>
        /// (Påskedag) Calculates easter sunday for the specified <paramref name="year"/>.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <returns>The <see cref="DateTime">date</see> of easter sunday.</returns>
        /// <remarks>This method uses the algorithm specified in the wikipedia article: <a href="http://en.wikipedia.org/wiki/Computus">Computus</a>.</remarks>
        internal DateTime CalculateEasterSunday(int year)
        {
            if (year > DateTime.MaxValue.Year)
            {
                throw new ArgumentException("year");
            }
            int a = year % 19;
            int b = year / 100;
            int c = year % 100;
            int d = b / 4;
            int e = b % 4;
            int f = (b + 8) / 25;
            int g = (b - f + 1) / 3;
            int h = (19 * a + b - d - g + 15) % 30;
            int i = c / 4;
            int k = c % 4;
            int l = (32 + 2 * e + 2 * i - h - k) % 7;
            int m = (a + 11 * h + 22 * l) / 451;
            int month = (h + l - 7 * m + 114) / 31;
            int day = ((h + l - 7 * m + 114) % 31) + 1;
            return new DateTime(year, month, day).Date;
        }
    }
}
